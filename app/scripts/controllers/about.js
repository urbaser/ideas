'use strict';

/**
 * @ngdoc function
 * @name ideasApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the ideasApp
 */
angular.module('ideasApp')
  .controller('AboutCtrl', function ($scope, serviciosService) {

    $scope.fecha = "";
    $scope.autores = [];
    $scope.presentadaPor = [];
    $scope.tituloIdea = "";
    $scope.descripcionIdea = "";
    $scope.origenIdea = "";
    $scope.areaNegocio = "";
    $scope.file = "";
    $scope.fileName = "";

    $scope.open = function(){
      $("#fileInput").click();
    }

    $("document").ready(function(){
      $("#fileInput").change(function() {
        if($("#fileInput")[0].files.length){
          $scope.file = $("#fileInput")[0].files[0];
          $scope.fileName = $("#fileInput")[0].files[0].name;
        }else{
          $scope.file = "";
          $scope.fileName = "";
        }
        $scope.$apply();
      });
    });

    $scope.nuevaIdea = function(){
      var fecha = $scope.fecha;
      if(fecha == ""){
        fecha = null;
      }
      var autores = "";
      for (var i = 0; i < $scope.autores.length; i++) {
        autores+=$scope.autores[i]+";";
      }
      var taskProperties = {
        '__metadata': {
          'type': 'SP.Data.IdeasListItem'
        },
        'Fecha': fecha,
        'Autores': autores,
        'Title': $scope.tituloIdea,
        'Descripcion': $scope.descripcionIdea,
        //'Campa_x00f1_a': $scope.sss,
        'Origen': $scope.origenIdea,
        'AreaNegocio': $scope.areaNegocio
        //'TipoActuacion': $scope.codEmpresa,
        //'Estado': $scope.codProyecto,
        //'Encaja': $scope.typeBuild,
        //'Actuacion': $scope.useRegime,
        //'ResultadoEvaluacion': $scope.boss,
        //'ComentariosEvaluacion': $scope.boss,
        //'FechaEvaluacion': $scope.boss
      };

      serviciosService.nuevaIdea(taskProperties).then(function(data) {
        var id = data.Id;
        if($scope.file != ""){
          serviciosService.archivoAdjuntoIdea(id, $scope.file, $scope.fileName).then(function(data) {
            $scope.envio_ok = true;
          }).catch(function(err) {
            $scope.envio_ok = false;
          });
        }else{
          $scope.envio_ok = true;
        }
      }).catch(function(err) {
        $scope.envio_ok = false;
      });

    }

  });
