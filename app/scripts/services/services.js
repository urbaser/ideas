'use strict';

/**
 * @ngdoc service
 * @name angularApp.cargarCompetidores
 * @description
 * # cargarCompetidores
 * Service in the angularApp.
 */
angular.module('ideasApp')
  .service('serviciosService', ['$http', '$q', 'CONSTANTES', function ($http, $q, CONSTANTES) {

    var desarrollo = true;
    var ip = CONSTANTES.IP;
    if(desarrollo){
      ip = CONSTANTES.IP_DESARROLLO;
    }

    var requestHeader = {
      get: {
        'headers': {
          'accept': 'application/json;odata=verbose'
        }
      }
    };

    return{
      getIndicadoresTotales: getIndicadoresTotales,
      nuevaIdea: nuevaIdea,
      archivoAdjuntoIdea: archivoAdjuntoIdea
    };

    function getIndicadoresTotales(){
      var url = ip+"_api/lists/getbytitle('Indicadores totales')/items?$top=5000";
      if(mocks){
        url = "././mocks/datosTotales.json";
      }
      var promise = $http.get(url, requestHeader.get)
      .then(function (response) {
        return response.data;
      });
      return promise;
    }

    function nuevaIdea(taskProperties){
      var defered = $q.defer();
      var promise = defered.promise;

      $.ajax({
        url: ip + "/_api/contextinfo",
        type: "POST",
        async: false,
        headers: {
          "Accept": "application/json;odata=verbose"
        },
        complete: function (data) {
          var requestDigest = data.responseJSON.d.GetContextWebInformation.FormDigestValue;

          $http({
            url: ip+"/_api/web/lists/getbytitle('Ideas')/items",
            method: 'POST',
            headers: {
              "Content-Type": "application/json;odata=verbose",
              "Accept": "application/json;odata=verbose",
              "X-RequestDigest": requestDigest
            },
            data: JSON.stringify(taskProperties),
          }).then(function successCallback(response) {
            defered.resolve(response.data.d);
          }, function errorCallback(response) {
            defered.reject('Error');
          });
        },
        error: function (err) {
          defered.reject('Error');
        }
      });

      return promise;
    }

    function archivoAdjuntoIdea(id, file, fileName){
      var defered = $q.defer();
      var promise = defered.promise;
      var requestDigest = "";

      $.ajax({
        url: ip+"/_api/contextinfo",
        type: "POST",
        async: false,
        headers: {
          "Accept": "application/json;odata=verbose"
        },
        complete: function (data) {
          var requestDigest = data.responseJSON.d.GetContextWebInformation.FormDigestValue;

          if(requestDigest != ""){
            var reader = new FileReader();
            reader.onloadend = function(evt){
              if (evt.target.readyState == FileReader.DONE){
                var buffer = evt.target.result;
                $.ajax({
                  url: ip+"/_api/web/lists/getbytitle('Ideas')/items("+id+")/AttachmentFiles/add(FileName='"+fileName+"')",
                  type: "POST",
                  data: buffer,
                  processData: false,
                  headers: {
                    "accept": "application/json;odata=verbose",
                    "X-RequestDigest": requestDigest
                  },
                  complete: function (data) {
                    defered.resolve('Bien');
                  },
                  error: function (err) {
                    defered.reject('Error');
                  }
                });
              }
            };
            reader.readAsArrayBuffer(file);
          }
        },
        error: function (err) {
          defered.reject('Error');
        }
      });

      return promise;
    }

  }]);
